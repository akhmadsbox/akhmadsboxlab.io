let cards = document.getElementsByClassName('projects-list-item')
let slides = document.getElementsByClassName('slide')
function moveCards (slide){
    console.log(cards[0].classList[1])
    console.log(slide)
    if(slide.classList.contains('slide-left')){
        console.log('left')
        if (cards[0].classList.contains('first-card')){
            cards[0].classList.remove('first-card')
            cards[0].classList.add('third-card')
            cards[1].classList.remove('second-card')
            cards[1].classList.add('first-card')
            cards[2].classList.remove('third-card')
            cards[2].classList.add('second-card')
        } else if (cards[0].classList.contains('second-card')) {
            cards[0].classList.remove('second-card')
            cards[0].classList.add('first-card')
            cards[1].classList.remove('third-card')
            cards[1].classList.add('second-card')
            cards[2].classList.remove('first-card')
            cards[2].classList.add('third-card')
        } else if (cards[0].classList.contains('third-card')){
            cards[0].classList.remove('third-card')
            cards[0].classList.add('second-card')
            cards[1].classList.remove('first-card')
            cards[1].classList.add('third-card')
            cards[2].classList.remove('second-card')
            cards[2].classList.add('first-card')
        }
    }
     else if(slide.classList.contains('slide-right')){
        console.log('right')
        if (cards[0].classList.contains('first-card')){
            cards[0].classList.remove('first-card')
            cards[0].classList.add('second-card')
            cards[1].classList.remove('second-card')
            cards[1].classList.add('third-card')
            cards[2].classList.remove('third-card')
            cards[2].classList.add('first-card')
        } else if (cards[0].classList.contains('second-card')) {
            cards[0].classList.remove('second-card')
            cards[0].classList.add('third-card')
            cards[1].classList.remove('third-card')
            cards[1].classList.add('first-card')
            cards[2].classList.remove('first-card')
            cards[2].classList.add('second-card')
        } else if (cards[0].classList.contains('third-card')){
            cards[0].classList.remove('third-card')
            cards[0].classList.add('first-card')
            cards[1].classList.remove('first-card')
            cards[1].classList.add('second-card')
            cards[2].classList.remove('second-card')
            cards[2].classList.add('third-card')
        }
    }
}

let navItem = document.getElementsByClassName('nav-list-item')
let projects = document.getElementsByClassName('projects')
let contact = document.getElementsByClassName('contact')
let navbar = document.getElementsByClassName('navbar')
let navbarMore = document.getElementsByClassName('navbar-more')
let navbarCross = document.getElementsByClassName('navbar-cross')
console.log(navbarMore[0])
navItem[0].addEventListener('click', () =>{
    navbar[0].classList.remove('selected')
    window.scrollTo({
        top:0
    })
})
navItem[1].addEventListener('click', () =>{
    navbar[0].classList.remove('selected')
    window.scrollTo({
        top: projects[0].offsetTop
    })
})
navItem[2].addEventListener('click', () =>{
    navbar[0].classList.remove('selected')
    window.scrollTo({
        top: contact[0].offsetTop
    })
})
window.addEventListener('scroll',()=>{
    // navbar[0].style.transform = `translateY(${window.scrollY}px)`
    if(window.scrollY > 30){
        navbar[0].classList.add('scrolled-navbar')
    } else {
        navbar[0].classList.remove('scrolled-navbar')
    }
})

navbarMore[0].addEventListener('click', ()=>{
    navbar[0].classList.add('selected')
    document.body.style.overflowY = 'hidden'
})
navbarCross[0].addEventListener('click', ()=>{
    navbar[0].classList.remove('selected')
    document.body.style.overflowY = 'visible'
})

